const container = document.getElementById('async');
const button = document.createElement('a');
button.className = "show-ip";
button.textContent = 'Вычислить по IP';
container.insertAdjacentElement('beforeend', button);
button.addEventListener('click', function (e) {
    e.preventDefault();
    const info = getPlaceByIp();
    info.then(result => {
        const ul = document.createElement('ul');
        ul.className = 'text-center';
        for(const [key, value] of Object.entries(result)){
            ul.insertAdjacentHTML("beforeend", `<li>${key}: ${value}</li>`)
        }
        this.closest('div').after(ul);
    })
});
async function getPlaceByIp() {
    const ipResponse = await fetch('https://api.ipify.org/?format=json');
    const {ip} = await ipResponse.json();
    const apiResponse = await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district&lang=ru`);
    const apiInfo = await apiResponse.json();
    return apiInfo
}